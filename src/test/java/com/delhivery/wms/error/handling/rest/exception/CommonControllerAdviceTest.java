
package com.delhivery.wms.error.handling.rest.exception;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;
import com.delhivery.wms.error.handling.rest.error.ErrorCode;
import com.delhivery.wms.error.handling.rest.error.ErrorResource;
import com.delhivery.wms.error.handling.rest.error.ErrorResultResource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.MethodParameter;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CommonControllerAdviceTest {

  static final String NOT_READABLE = "Could not read document: "
      + "Can not construct instance of java.util.UUID from String value "
      + "'d8973e94-910b-4edb-9e67-046e853e37221': "
      + "not a valid textual representation, problem: "
      + "UUID has to be represented by the standard 36-char representation";

  static final String UUID_FORMAT_ERROR =
      "UUID has to be represented by the " + "standard 36-char representation";

  private CommonControllerAdvice commonControllerAdvice;

  @Before
  public void setup() {
    commonControllerAdvice = new CommonControllerAdvice();
  }

  @Test
  public void ensureDefaultDetailsMessageForNotFoundExceptionWhenNoneSupplied() {
    NotFoundException iee = NotFoundException.builder().entityNotFound().build();
    ResponseEntity<Object> errorResult = commonControllerAdvice.handleException(iee);

    List<String> details = ((ErrorResultResource) errorResult.getBody()).getError().getDetails();
    assertEquals(details.get(0),
        "The ID(s) specified in the URL do not correspond to the values in the system.");
  }

  @Test
  public void ensureCustomDetailsMessageForNotFoundExceptionWhenSupplied() {
    NotFoundException iee =
        NotFoundException.builder().entityNotFound().details("Custom details").build();
    ResponseEntity<Object> errorResult = commonControllerAdvice.handleException(iee);

    List<String> details = ((ErrorResultResource) errorResult.getBody()).getError().getDetails();
    assertEquals(details.get(0), "Custom details");
  }

  @Test
  public void ensureDefaultDetailsMessageForInternalErrorExceptionWhenNoneSupplied() {
    InternalErrorException iee = InternalErrorException.builder().gatewayError().build();
    ResponseEntity<Object> errorResult = commonControllerAdvice.handleException(iee);

    List<String> details = ((ErrorResultResource) errorResult.getBody()).getError().getDetails();
    assertEquals(details.get(0), "Error communicating with downstream provider.");
  }

  @Test
  public void ensureCustomDetailsMessageForInternalErrorExceptionWhenSupplied() {
    InternalErrorException iee =
        InternalErrorException.builder().gatewayError().details("Custom details").build();
    ResponseEntity<Object> errorResult = commonControllerAdvice.handleException(iee);

    List<String> details = ((ErrorResultResource) errorResult.getBody()).getError().getDetails();
    assertEquals(details.get(0), "Custom details");
  }

  @Test
  public void ensureDefaultDetailsMessageForBadRequestExceptionWhenNoneSupplied() {
    BadRequestException bre = BadRequestException.builder().invalidCurrency().build();
    ResponseEntity<Object> errorResult = commonControllerAdvice.handleException(bre);

    List<String> details = ((ErrorResultResource) errorResult.getBody()).getError().getDetails();
    assertEquals(details.get(0),
        "The currency code is invalid or your account does not support this currency.");
  }

  @Test
  public void ensureCustomDetailsMessageForBadRequestExceptionWhenSupplied() {
    BadRequestException bre =
        BadRequestException.builder().invalidCurrency().details("Custom details").build();
    ResponseEntity<Object> errorResult = commonControllerAdvice.handleException(bre);

    List<String> details = ((ErrorResultResource) errorResult.getBody()).getError().getDetails();
    assertEquals(details.get(0), "Custom details");
  }

  @Test
  public void ensureDefaultDetailsMessageForExternalGatewayErrorExceptionWhenNoneSupplied() {
    ExternalGatewayErrorException bre =
        ExternalGatewayErrorException.builder().gatewayError().build();
    ResponseEntity<Object> errorResult = commonControllerAdvice.handleException(bre);

    List<String> details = ((ErrorResultResource) errorResult.getBody()).getError().getDetails();
    assertEquals(details.get(0), "An external gateway error occurred.");
  }

  @Test
  public void ensureCustomDetailsMessageForExternalGatewayErrorExceptionWhenSupplied() {
    ExternalGatewayErrorException bre =
        ExternalGatewayErrorException.builder().gatewayError().details("Custom details").build();
    ResponseEntity<Object> errorResult = commonControllerAdvice.handleException(bre);

    List<String> details = ((ErrorResultResource) errorResult.getBody()).getError().getDetails();
    assertEquals(details.get(0), "Custom details");
  }

  @Test
  public void ensureDefaultDetailsMessageForUnauthorizedExceptionWhenNoneSupplied() {
    UnauthorizedException ue = UnauthorizedException.builder().requestNotParsable().build();
    ResponseEntity<Object> errorResult = commonControllerAdvice.handleException(ue);

    List<String> details = ((ErrorResultResource) errorResult.getBody()).getError().getDetails();
    assertEquals(details.get(0), "The authentication credentials are invalid.");
  }

  @Test
  public void ensureCustomDetailsMessageForUnauthorizedExceptionWhenSupplied() {
    UnauthorizedException ue =
        UnauthorizedException.builder().requestNotParsable().details("Custom details").build();
    ResponseEntity<Object> errorResult = commonControllerAdvice.handleException(ue);

    List<String> details = ((ErrorResultResource) errorResult.getBody()).getError().getDetails();
    assertEquals(details.get(0), "Custom details");
  }

  @Test
  public void testUuid_InvalidFormatException() {
    HttpMessageNotReadableException hmnre = new HttpMessageNotReadableException(NOT_READABLE);

    Throwable nfe = new NumberFormatException(UUID_FORMAT_ERROR);
    InvalidFormatException ife = new InvalidFormatException(null, "Test", nfe, UUID.class);
    hmnre.initCause(ife);
    ResponseEntity<Object> errorResult =
        commonControllerAdvice.handleHttpMessageNotReadable(hmnre, null, null, null);

    assertEquals(
        ((ErrorResultResource) errorResult.getBody()).getError().getFieldErrors().get(0).getError(),
        "Invalid format for UUID java.lang.NumberFormatException: " + UUID_FORMAT_ERROR);
    assertEquals(HttpStatus.BAD_REQUEST, errorResult.getStatusCode());
  }

  @Test
  public void testInvalidFormatExceptionForSimpleEnumName() {
    InvalidFormatException ife = mock(InvalidFormatException.class);

    when(ife.getPath()).thenReturn(getReferenceList("country"));

    String response = CommonControllerAdvice.retrieveInvalidFormatExceptionFieldName(ife);

    assertEquals("country", response);
  }

  @Test
  public void testInvalidFormatExceptionForComplexEnumName() {
    InvalidFormatException ife = mock(InvalidFormatException.class);

    when(ife.getPath()).thenReturn(getReferenceList("billing", "address", "country"));

    String response = CommonControllerAdvice.retrieveInvalidFormatExceptionFieldName(ife);

    assertEquals("billing.address.country", response);
  }

  private static List<Reference> getReferenceList(String... args) {
    List<Reference> paths = new ArrayList<>();

    for (String s : args) {
      Reference reference = new Reference(s, s.toString());
      paths.add(reference);
    }

    return paths;
  }

  @Test
  public void validateResponseForHttpMediaTypeNotSupportedException() {
    HttpMediaTypeNotSupportedException exception = new HttpMediaTypeNotSupportedException("test");
    ResponseEntity<Object> errorResult =
        commonControllerAdvice.handleHttpMediaTypeNotSupported(exception, null, null, null);

    assertEquals("5272", ((ErrorResultResource) errorResult.getBody()).getError().getCode());
    assertEquals(HttpStatus.UNSUPPORTED_MEDIA_TYPE, errorResult.getStatusCode());
    assertEquals("Unsupported 'Content-Type'",
        ((ErrorResultResource) errorResult.getBody()).getError().getMessage());
    assertEquals("The 'Content-Type' in the request header is an unsupported format.",
        ((ErrorResultResource) errorResult.getBody()).getError().getDetails().get(0));
  }

  @Test
  public void validateResponseForHttpMediaTypeNotAcceptableException() {
    HttpMediaTypeNotAcceptableException exception = new HttpMediaTypeNotAcceptableException("test");
    ResponseEntity<Object> errorResult =
        commonControllerAdvice.handleHttpMediaTypeNotAcceptable(exception, null, null, null);

    /*assertEquals("{\"error\":{\"code\":\"5271\",\"message\":"
        + "\"Unsupported 'Accept' header\",\"details\":["
        + "\"You requested a response in the 'Accept' header that is in an unsupported format.\""
        + "]}}", errorResult.getBody());*/
    assertEquals(HttpStatus.NOT_ACCEPTABLE, errorResult.getStatusCode());
  }

  @Test
  public void validateResponseForHttpMessageNotReadableException() {
    HttpMessageNotReadableException exception = new HttpMessageNotReadableException("test");
    ResponseEntity<Object> errorResult =
        commonControllerAdvice.handleHttpMessageNotReadable(exception, null, null, null);

    assertEquals("5023", ((ErrorResultResource) errorResult.getBody()).getError().getCode());
    assertEquals(HttpStatus.BAD_REQUEST, errorResult.getStatusCode());
    assertEquals("Request body not parsable",
        ((ErrorResultResource) errorResult.getBody()).getError().getMessage());
    assertEquals("The request is not parseable.",
        ((ErrorResultResource) errorResult.getBody()).getError().getDetails().get(0));
  }

  @Test
  public void validateResponseForMethodArgumentNotValidException() throws Exception {

    // GIVEN
    MethodArgumentNotValidException exception = new MethodArgumentNotValidException(
        new MethodParameter(
            String.class.getDeclaredMethod("concat", String.class),
            0
        ),
        new DirectFieldBindingResult("", "java.lang.String")
    );

    // WHEN
    ResponseEntity<Object> errorResult =
        commonControllerAdvice.handleMethodArgumentNotValid(exception, null, null, null);
    ErrorResource actual = ((ErrorResultResource) errorResult.getBody()).getError();

    final String expectedMessage = "Either you submitted a request that is missing a mandatory field or the value of a "
        + "field does not match the format expected.";

    // THEN
    assertEquals("5068", actual.getCode());
    assertEquals(HttpStatus.BAD_REQUEST, errorResult.getStatusCode());
    assertEquals("Field error(s)", actual.getMessage());
    assertEquals(expectedMessage, actual.getDetails().get(0));
  }

  @Test
  public void validateResponseForTypeMismatchException() {
    TypeMismatchException exception = new TypeMismatchException(new String(), String.class);
    ResponseEntity<Object> errorResult =
        commonControllerAdvice.handleTypeMismatch(exception, null, null, null);

    assertEquals("5023", ((ErrorResultResource) errorResult.getBody()).getError().getCode());
    assertEquals(HttpStatus.BAD_REQUEST, errorResult.getStatusCode());
    assertEquals("Request body not parsable",
        ((ErrorResultResource) errorResult.getBody()).getError().getMessage());
    String errorDetails =
        ((ErrorResultResource) errorResult.getBody()).getError().getDetails().get(0);
    assertThat(errorDetails, containsString("Failed to convert value of type"));
  }

  /**
   * This occurs when attempting a PUT when only POST and GET are supported; as an example.
   */
  @Test
  public void validateResponseForHandleHttpRequestMethodNotSupported() {
    HttpRequestMethodNotSupportedException exception =
        new HttpRequestMethodNotSupportedException("PUT");
    ResponseEntity<Object> errorResult =
        commonControllerAdvice.handleHttpRequestMethodNotSupported(exception, null, null, null);

    assertEquals("5281", ((ErrorResultResource) errorResult.getBody()).getError().getCode());
    assertEquals(HttpStatus.METHOD_NOT_ALLOWED, errorResult.getStatusCode());
    assertEquals("Not supported",
        ((ErrorResultResource) errorResult.getBody()).getError().getMessage());
    assertEquals("Request method 'PUT' not supported",
        ((ErrorResultResource) errorResult.getBody()).getError().getDetails().get(0));
  }

  @Test
  public void validateResponseForUncaughtRuntimeException() {
    RuntimeException exception = new RuntimeException();
    ResponseEntity<Object> errorResult = commonControllerAdvice.handleException(exception);

    assertEquals("1000", ((ErrorResultResource) errorResult.getBody()).getError().getCode());
    assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, errorResult.getStatusCode());
    assertEquals("Internal Error",
        ((ErrorResultResource) errorResult.getBody()).getError().getMessage());
    assertEquals("An internal error occurred.",
        ((ErrorResultResource) errorResult.getBody()).getError().getDetails().get(0));
  }

  @Test
  public void properSelfLinkListGenerated() {
    Link selfLink = new Link("self", "href");
    WmsGlobalException WmsGlobalException =
        BadRequestException.builder().errorCode(CommonErrorCode.EXTERNAL_GATEWAY_ERROR)
            .selfLinks(selfLink).build();

    WmsGlobalException
        .setSelfLinks(new Link("self1", "href1"), new Link("self2", "href2"), new Link("self3", "href3"));
    //WmsGlobalException.setSelfLink(selfLink);

    ResponseEntity<Object> response = commonControllerAdvice.handleException(WmsGlobalException);

    ErrorResultResource errorResult = (ErrorResultResource) response.getBody();

    assertNotNull(errorResult.getLinks().get(0));
    //assertEquals(WmsGlobalException.getSelfLink(), selfLink);
  }

  @Test
  public void nullSelfLinkListGenerated() {
    WmsGlobalException WmsGlobalException =
        BadRequestException.builder().errorCode(CommonErrorCode.EXTERNAL_GATEWAY_ERROR).build();

    ResponseEntity<Object> response = commonControllerAdvice.handleException(WmsGlobalException);

    ErrorResultResource errorResult = (ErrorResultResource) response.getBody();

    assertNull(errorResult.getLinks());
  }

  @Test
  public void testNonNullDetailsInCaseOfNullDescription() {
    ErrorCode errorCode = new ErrorCode() {

      @Override
      public String getCode() {
        return "testNullDescription";
      }

      @Override
      public String getMessage() {
        return "testNullDescriptionMessage";
      }

      @Override
      public String getDescription() {
        return null;
      }
      
    };
    
    WmsGlobalException WmsGlobalException =
        InternalErrorException.builder().errorCode(errorCode).build();
    ResponseEntity<Object> response = commonControllerAdvice.handleException(WmsGlobalException);
    ErrorResultResource errorResult = (ErrorResultResource) response.getBody();
    assertNull(errorResult.getError().getDetails());
  }

  @Test
  public void testNonNullDetailsWithFieldErrorsInCaseOfNullDescription() {
    ErrorCode errorCode = new ErrorCode() {

      @Override
      public String getCode() {
        return "testNullDescription";
      }

      @Override
      public String getMessage() {
        return "testNullDescriptionMessage";
      }

      @Override
      public String getDescription() {
        return null;
      }
      
    };
    List<FieldError> fieldErrors = new ArrayList<>();
    FieldError error = new FieldError("", "dummy", "dummy");
    fieldErrors.add(error);
    
    class TestException extends WmsGlobalException {
      public TestException(HttpStatus status, ErrorCode errorCode, List<FieldError> fieldErrorList, Throwable cause) {
        super(status, errorCode, fieldErrorList, cause);
      }
    }

    TestException testException = new TestException(HttpStatus.INTERNAL_SERVER_ERROR, errorCode, fieldErrors, null);

    ResponseEntity<Object> response = commonControllerAdvice.handleException(testException);
    ErrorResultResource errorResult = (ErrorResultResource) response.getBody();
    assertNull(errorResult.getError().getDetails());
  }
}
