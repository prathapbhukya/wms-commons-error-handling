
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;
import com.delhivery.wms.error.handling.rest.error.ErrorCode;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Use this exception for the case when receiving an error response from external party that should
 * return http status code 502.
 * 
 *
 */
@ResponseStatus(value = HttpStatus.BAD_GATEWAY, reason = "An error occured in the external gateway")
public class ExternalGatewayErrorException extends WmsGlobalException {

  private static final long serialVersionUID = -6396552431370118927L;

  public ExternalGatewayErrorException(ErrorCode errorCode, Throwable cause) {
    super(HttpStatus.BAD_GATEWAY, errorCode, cause);
  }

  public ExternalGatewayErrorException(ErrorCode errorCode) {
    this(errorCode, null);
  }

  static ExternalGatewayErrorException newInstance(ExternalGatewayErrorException.Builder builder) {
    ExternalGatewayErrorException externalGatewayErrorException =
        new ExternalGatewayErrorException(builder.errorCode, builder.cause);
    if (ArrayUtils.isNotEmpty(builder.getDetails())) {
      externalGatewayErrorException.setDetails(builder.getDetails());
    }
    return externalGatewayErrorException;
  }

  /**
   * Builder to create an ExternalGatewayErrorException.
   * 
   * @return ExternalGatewayErrorException.Builder
   */
  public static Builder builder() {
    return new ExternalGatewayErrorException.Builder();
  }

  public static class Builder
      extends WmsGlobalException.AbstractBuilder<ExternalGatewayErrorException, Builder> {

    /**
     * Used for general error that caused by external gateway.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#EXTERNAL_GATEWAY_ERROR
     */
    public Builder gatewayError() {
      return errorCode(CommonErrorCode.EXTERNAL_GATEWAY_ERROR);
    }

    @Override
    public ExternalGatewayErrorException build() {
      return newInstance(this);
    }
  }
}
