
package com.delhivery.wms.error.handling.rest.exception;

import static com.delhivery.wms.error.handling.rest.exception.CommonControllerAdvice.ORDER;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import com.netflix.hystrix.exception.HystrixRuntimeException;

import com.delhivery.wms.error.handling.rest.error.ErrorResultResource;
import com.delhivery.wms.error.handling.util.Constants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Common exception handler to handle REST errors consistently.
 */
@ControllerAdvice
@Order(ORDER)
public class CommonControllerAdvice extends ResponseEntityExceptionHandler {

  private static final Logger logger = LoggerFactory.getLogger(CommonControllerAdvice.class);

  static final int ORDER = Ordered.LOWEST_PRECEDENCE;

  @Override
  protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body,
      HttpHeaders headers, HttpStatus status, WebRequest request) {

    
    // Once we are upgraded to Spring 4.3.+, then this if should be able to go away.
    if (HttpStatus.NOT_FOUND.equals(status) && ex instanceof NoHandlerFoundException) {
      WmsGlobalException wge = NotFoundException.builder().uriNotFound().build();
      return createResponseEntity(wge);
    } else {
      WmsGlobalException wge = InternalErrorException.builder().internalError().cause(ex).build();
      wge.setDetails(ex.getMessage());
      return createResponseEntity(wge, status);
    }
  }

  /**
   * Thrown when the incorrect content type is specified.
   *
   * @param ex - When the incorrect content type is specified
   * @return a standard ErrorResult
   */
  @Override
  protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
      HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status,
      WebRequest request) {

    WmsGlobalException wge = new UnsupportedContentTypeException();
    return createResponseEntity(wge);
  }

  /**
   * You requested a response in the 'Accept' header that is in an unsupported format.
   *
   * [Warning] We have to make the ResponseEntity a String type to have this handler called
   * otherwise it gets ignored.
   *
   * @param ex - When an unsupported format is specified in the 'Accept' header
   * @return a standard ErrorResult
   */
  @Override
  protected ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(
      HttpMediaTypeNotAcceptableException ex, HttpHeaders headers, HttpStatus status,
      WebRequest request) {
    WmsGlobalException wge = new UnsupportedResponseFormatException();

    ResponseEntity<Object> resultResponseEntity = createResponseEntity(wge);
    ErrorResultResource result = (ErrorResultResource) resultResponseEntity.getBody();
    ObjectWriter objectWriter = new ObjectMapper().writer();
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    // Default the response in case our parsing fails. Should never happen.
    String response = "{\n" + "    \"error\": {\n" + "        \"code\": \"5271\",\n"
        + "        \"message\": \" Cannot return requested format. \", \n"
        + "        \"details\": [\n" + "            \" %s \"\n" + "        ]\n" + "    }\n" + "}";

    try {
      response = objectWriter.writeValueAsString(result);
    } catch (JsonProcessingException e) {
      logger.error("Could not create response", e);
      // Ignore and return default error if we cannot parse
    }

    return new ResponseEntity<>(response, httpHeaders, resultResponseEntity.getStatusCode());
  }

  /**
   * When the JSON body cannot be parsed.
   *
   * @param hmnpe HttpMessageNotReadableException caused by not being able to parse the JSON body
   * @return a standard ErrorResult
   */
  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    logger.warn("handleHttpMessageNotReadable", ex);
    if (ex.getCause() instanceof InvalidFormatException) {
      InvalidFormatException invalidFormatException = (InvalidFormatException) ex.getCause();
      if (isTargetTypeEnum(invalidFormatException)) {
        return handleInvalidFormatEnumException(invalidFormatException);
      }
      if (isTargetTypeUuid(invalidFormatException)) {
        return handleInvalidUuidFormatException(invalidFormatException);
      }
    }
    return createResponseEntity(
        BadRequestException.builder().requestNotParsable().cause(ex).build());
  }

  /**
   * When there are invalid or missing fields.
   *
   * @param ex method argument not valid, this exception contains field errors defined by the spring
   *          data binding framework
   * @return a standard ErrorResult
   */
  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {

    List<FieldError> fieldErrorList = ex.getBindingResult().getFieldErrors();
    WmsGlobalException wge = new InvalidFieldException(fieldErrorList);

    return createResponseEntity(wge);
  }

  /**
   * Handles all one platform exceptions with consistent responses and details.
   *
   * @param ex http request method not supported exception
   * @return a standard ErrorResult
   */
  @Override
  protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
      HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status,
      WebRequest request) {

    WmsGlobalException wge = new NotSupportedException();
    wge.setDetails(ex.getMessage());
    return createResponseEntity(wge);
  }

  /**
   * Handles hystrix runtime exceptions with consistent responses and details.
   *
   * @param ex an unexpected exception
   * @return a standard ErrorResult
   */
  @ExceptionHandler(HystrixRuntimeException.class)
  public final ResponseEntity<Object> handleHystrixRuntimeException(HystrixRuntimeException ex) {
    logger.error(String.format("Caught hystrix runtime exception: %s", ex.getMessage()));
    WmsGlobalException wge = InternalErrorException.builder().internalError().cause(ex).build();
    return createResponseEntity(wge);
  }

  /**
   * Handles all one platform exceptions with consistent responses and details.
   *
   * @param wge One platform exception
   * @return a standard ErrorResult
   */
  @ExceptionHandler(WmsGlobalException.class)
  public final ResponseEntity<Object> handleException(WmsGlobalException wme) {
    logger.warn("Caught Wms Global exception: ", wme);
    return createResponseEntity(wme);
  }

  /**
   * Handles uncaught runtime exceptions with consistent responses and details.
   *
   * @param ex an unexpected exception
   * @return a standard ErrorResult
   */
  @ExceptionHandler(Exception.class)
  public final ResponseEntity<Object> handleException(Exception ex) {
    logger.warn("Caught unexpected exception: ", ex);
    WmsGlobalException wge = InternalErrorException.builder().internalError().cause(ex).build();
    return createResponseEntity(wge);
  }

  /**
   * When there is an incorrect or missing variable on an http get.
   */
  @Override
  protected ResponseEntity<Object> handleMissingServletRequestParameter(
      MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status,
      WebRequest request) {

    final List<FieldError> fieldErrors = new ArrayList<>();
    fieldErrors.add(new FieldError("object", ex.getParameterName(), "Value is required."));
    WmsGlobalException wge = new InvalidFieldException(fieldErrors);

    return createResponseEntity(wge);
  }

  /**
   * Handle the case where we we get a type mismatch when passing path parameters in the URL.
   *
   * @param ex the type mismatch exception
   * @return a standard ErrorResult
   */
  @Override
  protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
      HttpStatus status, WebRequest request) {

    if (ex.getMostSpecificCause() instanceof IllegalArgumentException) {
      WmsGlobalException wge = NotFoundException.builder().entityNotFound()
          .details(ex.getMostSpecificCause().getMessage()).build();
      return createResponseEntity(wge);
    }

    WmsGlobalException wge = BadRequestException.builder().requestNotParsable()
        .details(ex.getMostSpecificCause().getMessage()).build();

    return createResponseEntity(wge);
  }

  /**
   * Construct the response from a wms global Exception.
   */
  static ResponseEntity<Object> createResponseEntity(WmsGlobalException wge) {
    return createResponseEntity(wge, wge.getStatus());
  }

  /**
   * Construct the response from a wms global Exception.
   */
  private static ResponseEntity<Object> createResponseEntity(WmsGlobalException wge,
      HttpStatus httpStatus) {

    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(Constants.X_APPLICATION_ERROR_CODE, wge.getErrorCode().getCode());
    responseHeaders.set(Constants.X_APPLICATION_ERROR_CODE_LEGACY, wge.getErrorCode().getCode());
    responseHeaders.set(Constants.X_APPLICATION_ERROR_MESSAGE, wge.getErrorCode().getMessage());
    responseHeaders.set(Constants.X_APPLICATION_ERROR_DESCRIPTION,
        wge.getErrorCode().getDescription());

    ErrorResultResource errorResult = ErrorResultResource.from(wge);

    return new ResponseEntity<>(errorResult, responseHeaders, httpStatus);
  }

  private static boolean isTargetTypeUuid(InvalidFormatException invalidFormatException) {
    String targetType = invalidFormatException.getTargetType().getSimpleName();
    return null != targetType && "UUID".equals(targetType);
  }

  private ResponseEntity<Object>
      handleInvalidUuidFormatException(InvalidFormatException invalidFormatException) {
    String invalidUuid = invalidFormatException.getValue().toString();
    String message = String.format("Invalid format for UUID %s", invalidUuid);
    List<FieldError> fieldErrorList = new ArrayList<>();
    fieldErrorList.add(new FieldError(invalidUuid,
        retrieveInvalidFormatExceptionFieldName(invalidFormatException), message));
    return createResponseEntity(new InvalidFieldException(fieldErrorList));

  }

  private static boolean isTargetTypeEnum(InvalidFormatException invalidFormatException) {
    final Class<?> targetType = invalidFormatException.getTargetType();
    return targetType != null && targetType.isEnum();
  }

  private ResponseEntity<Object>
      handleInvalidFormatEnumException(InvalidFormatException invalidFormatException) {
    final Class<?> targetType = invalidFormatException.getTargetType();
    List<FieldError> fieldErrorList = retrieveFieldErrorList(invalidFormatException, targetType);
    return createResponseEntity(new InvalidFieldException(fieldErrorList));
  }

  private static List<FieldError> retrieveFieldErrorList(
      InvalidFormatException invalidFormatException, final Class<?> targetType) {
    String invalidValue = invalidFormatException.getValue().toString();
    String msg = String.format("invalid value '" + invalidValue + "', valid values are %s",
        Arrays.asList(targetType.getEnumConstants()));
    List<FieldError> fieldErrorList = new ArrayList<>();
    fieldErrorList.add(new FieldError(invalidValue,
        retrieveInvalidFormatExceptionFieldName(invalidFormatException), msg));
    return fieldErrorList;
  }

  static String retrieveInvalidFormatExceptionFieldName(final InvalidFormatException ifx) {
    final StringBuilder sb = new StringBuilder();

    List<Reference> referenceList = ifx.getPath();
    if (referenceList != null) {
      boolean isFirst = true;
      for (Reference ref : referenceList) {
        if (isFirst) {
          isFirst = false;
        } else {
          sb.append('.');
        }
        sb.append(ref.getFieldName());
      }
    }

    return sb.toString();
  }

}
