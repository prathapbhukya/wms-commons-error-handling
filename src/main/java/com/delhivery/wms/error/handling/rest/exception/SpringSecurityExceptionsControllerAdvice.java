
package com.delhivery.wms.error.handling.rest.exception;

import static com.delhivery.wms.error.handling.rest.exception.CommonControllerAdvice.createResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;
import com.delhivery.wms.error.handling.rest.error.ErrorResultResource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

/**
 * Exception handler for spring-security specific errors.
 */
@ConditionalOnClass(AccessDeniedException.class)
@ControllerAdvice
@Order(CommonControllerAdvice.ORDER - 1)
public class SpringSecurityExceptionsControllerAdvice {

  private static final Logger logger = LoggerFactory.getLogger(SpringSecurityExceptionsControllerAdvice.class);

  /**
   * To customize response if user is not authorized which results into AccessDeniedException.
   *
   * @param ex AccessDeniedException
   * @param request WebRequest
   * @return Customized response for AccessDeniedException
   */
  @ExceptionHandler(AccessDeniedException.class)
  public ResponseEntity<Object> handleAccessDeniedException(
      final Exception ex,
      final WebRequest request) {

    final WmsGlobalException wge = new UnauthorizedAccessException();
    final ResponseEntity<Object> resultResponseEntity = createResponseEntity(wge);
    final ErrorResultResource result = (ErrorResultResource) resultResponseEntity.getBody();
    final ObjectWriter objectWriter = new ObjectMapper().writer();

    // @formatter:off
    String response =
        "{\n" + "    \"error\": {\n" + "        \"code\": \""
                                      + CommonErrorCode.UNAUTHORIZED_ACCESS.getCode() + "\",\n"
            + "        \"message\": \""
                                      + CommonErrorCode.UNAUTHORIZED_ACCESS.getMessage() + "\", \n"
            + "        \"details\": [\n" + "            \""
                                      + CommonErrorCode.UNAUTHORIZED_ACCESS.getDescription()
                                      + "\"\n" + "        ]\n" + "    }\n" + "}";
    // @formatter:on

    try {
      response = objectWriter.writeValueAsString(result);
    } catch (final JsonProcessingException e) {
      logger.error("Could not create response", e);
      // Ignore and return default error if we cannot parse
    }

    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.set("Content-Type", "application/json");

    return new ResponseEntity<>(response, httpHeaders, HttpStatus.FORBIDDEN);
  }
}
