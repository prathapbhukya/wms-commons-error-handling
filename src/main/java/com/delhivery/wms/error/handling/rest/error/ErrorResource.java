
package com.delhivery.wms.error.handling.rest.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;

/**
 * A standard error response when there are issue with a REST API request.
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
public class ErrorResource {

  private String code;
  private String message;
  private List<String> details;
  private List<FieldErrorResource> fieldErrors;

  @JsonProperty("links")
  @JsonInclude(Include.NON_EMPTY)
  protected List<Link> links;

  /**
   * A error response for the REST API. Default constructor
   */
  ErrorResource() {
    // Make this package scope, should be created with the builder
  }

  /**
   * Gets the value of the code property.
   *
   * @return possible object is {@link String }
   */
  public String getCode() {
    return code;
  }

  /**
   * Sets the value of the code property.
   *
   * @param code allowed object is {@link String }
   */
  public void setCode(String code) {
    this.code = code;
  }

  /**
   * Gets the value of the message property.
   *
   * @return possible object is {@link String }
   */
  public String getMessage() {
    return message;
  }

  /**
   * Sets the value of the message property.
   *
   * @param message allowed object is {@link String }
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * The list of detailed error messages.
   * 
   * @return a list of detailed error messages
   */
  public List<String> getDetails() {

    return this.details;
  }

  /**
   * Sets the error details.
   * 
   * @param details the detailed error message
   * 
   */
  public void setDetails(List<String> details) {
    this.details = details;
  }

  /**
   * Lists the fields that have errors.
   * 
   * @return the list of fields with errors
   */
  public List<FieldErrorResource> getFieldErrors() {

    return fieldErrors;
  }

  /**
   * The list of fields that have errors.
   * 
   * @param fieldErrors set the fields that have errors
   */
  public void setFieldErrors(List<FieldError> fieldErrors) {

    if (fieldErrors != null) {
      List<FieldErrorResource> fieldErrorList = new ArrayList<>();
      for (FieldError fieldError : fieldErrors) {
        FieldErrorResource fer = new FieldErrorResource();
        fer.setField(fieldError.getField());
        fer.setError(fieldError.getDefaultMessage());
        fieldErrorList.add(fer);
      }
      this.fieldErrors = fieldErrorList;
    }
  }

}