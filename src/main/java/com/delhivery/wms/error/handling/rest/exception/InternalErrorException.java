
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;
import com.delhivery.wms.error.handling.rest.error.ErrorCode;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Common internal error exception. Use the builder to create instances of this class, it supports
 * the following error codes:
 * 
 * {@link CommonErrorCode#INTERNAL_ERROR} {@link CommonErrorCode#DATABASE_ERROR}
 * {@link CommonErrorCode#CRYPTOGRAPHIC_ERROR} {@link CommonErrorCode#GATEWAY_ERROR}
 * 
 * 
 * Usage:
 * 
 * [source,java] -- return InternalErrorException .builder() .internalError() .cause(new
 * NullPointerException("NPE")) .details("bad error"); --
 *
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "An internal error occured.")
public class InternalErrorException extends WmsGlobalException {
  private static final long serialVersionUID = 1L;

  /**
   * Constructs a new exception with the specified error code and cause.
   * 
   * Note that the detail message associated with {@code cause} is <i>not</i> automatically
   * incorporated in this runtime exception's detail message.
   * 
   * @param errorCode common error code
   * @param cause the cause (which is saved for later retrieval by the {@link #getCause()} method).
   *          (A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or
   *          unknown.)
   */
  private InternalErrorException(ErrorCode errorCode, Throwable cause) {
    super(HttpStatus.INTERNAL_SERVER_ERROR, errorCode, cause);
  }

  /**
   * Construct a new instance of the exception with the builder.
   * 
   * @param builder used to create this exception
   * @return BadRequestException
   */
  static InternalErrorException newInstance(InternalErrorException.Builder builder) {
    InternalErrorException internalErrorException =
        new InternalErrorException(builder.errorCode, builder.cause);
    if (ArrayUtils.isNotEmpty(builder.getDetails())) {
      internalErrorException.setDetails(builder.getDetails());
    }
    return internalErrorException;
  }

  /**
   * Builder to create an InternalErrorException.
   * 
   * @return InternalErrorException.Builder
   */
  public static Builder builder() {
    return new InternalErrorException.Builder();
  }

  /**
   * Builder to create an internal error exception with the specific return code.
   * 
   * Example:
   * 
   * [source,java] -- return InternalErrorException .builder() // Other options are databaseError()
   * | cryptographicError() | gatewayError() .internalError() .cause(new
   * NullPointerException("NPE")) .details("bad error"); --
   *
   */
  public static class Builder
      extends WmsGlobalException.AbstractBuilder<InternalErrorException, Builder> {

    /**
     * Generic internal error that returns a consistent code, message and description.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#INTERNAL_ERROR
     */
    public Builder internalError() {
      return errorCode(CommonErrorCode.INTERNAL_ERROR);
    }

    /**
     * Error with database operations.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#DATABASE_ERROR
     */
    public Builder databaseError() {
      return errorCode(CommonErrorCode.DATABASE_ERROR);
    }

    /**
     * Error with cryptographic functions like encryption or decryption.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#CRYPTOGRAPHIC_ERROR
     */
    public Builder cryptographicError() {
      return errorCode(CommonErrorCode.CRYPTOGRAPHIC_ERROR);
    }

    /**
     * Error calling downstream providers.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#GATEWAY_ERROR
     */
    public Builder gatewayError() {
      return errorCode(CommonErrorCode.GATEWAY_ERROR);
    }

    /**
     * Sets the appropriate return code when constructing the exception.
     * 
     * @return a InternalErrorException with the correct error code
     */
    @Override
    public InternalErrorException build() {
      return newInstance(this);
    }

  }

}
