
package com.delhivery.wms.error.handling.util;

/**
 * Constants for defining profiles.
 */
public final class Constants {

  public static final String SYSTEM_ACCOUNT = "system";

  // Used for putting the error code, message and description into the header for the
  // RestAuditFilter to retrieve and store in the rest_txns table
  public static final String X_APPLICATION_ERROR_CODE = "X-Application-Error-Code";
  public static final String X_APPLICATION_ERROR_MESSAGE = "X-Application-Error-Message";
  public static final String X_APPLICATION_ERROR_DESCRIPTION = "X-Application-Error-Description";

  public static final String X_APPLICATION_ERROR_CODE_LEGACY = "X-Application-Status-Code";

  private Constants() {

  }
}