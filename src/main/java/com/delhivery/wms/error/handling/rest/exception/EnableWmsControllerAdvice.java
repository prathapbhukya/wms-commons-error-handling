
package com.delhivery.wms.error.handling.rest.exception;

import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Enables a common exception handling to provide a consistent error response.
 *
 * @author prathap
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({
    CommonControllerAdvice.class,
    ExceptionHandlerFilter.class,
    SpringSecurityExceptionsControllerAdvice.class
})
public @interface EnableWmsControllerAdvice {

}
