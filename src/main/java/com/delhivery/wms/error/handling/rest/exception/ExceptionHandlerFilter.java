
package com.delhivery.wms.error.handling.rest.exception;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.delhivery.wms.error.handling.rest.error.ErrorResultResource;
import com.delhivery.wms.error.handling.util.Constants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import java.io.IOException;

import javax.annotation.Priority;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Any exception that occurs in a OncePerRequestFilter happens before spring mvc gets control.
 * Therefore any attempt at exception handling with @ControllerAdvice will not work, and the default
 * spring exception will be returned as the response.
 *
 * Therefore, this filter exists simply to execute first, then forward the chain and catch any
 * runtime exceptions. This allows it to format the response properly; which as done as an internal
 * error exception.
 */
@Priority(value = Integer.MIN_VALUE)
public class ExceptionHandlerFilter extends OncePerRequestFilter {
  private static final Logger logger = LoggerFactory.getLogger(ExceptionHandlerFilter.class);
  private static ObjectMapper objectMapper = new ObjectMapper();

  @Override
  public void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {
    try {
      filterChain.doFilter(request, response);
    } catch (WmsGlobalException wge) {
      handleWmsGlobalException(response, wge, wge);
    } catch (MaxUploadSizeExceededException exe) {
      WmsGlobalException wge = BadRequestException.builder().maxUploadSizeExceeded().cause(exe).build();
      handleWmsGlobalException(response, exe, wge);
    } catch (RuntimeException e) {
      WmsGlobalException wge;
      if (e.getCause() instanceof JsonParseException) {
        wge = BadRequestException.builder().requestNotParsable().cause(e).build();
      } else {
        logger.error("Internal error", e);
        wge = InternalErrorException.builder().internalError().cause(e).build();
      }
      handleWmsGlobalException(response, e, wge);
    }
  }

  private void handleWmsGlobalException(HttpServletResponse response, RuntimeException rte,
      WmsGlobalException wge) {

    ErrorResultResource errorResult =
        ErrorResultResource.builder()
            .id(wge.getId())
            .errorCode(wge.getErrorCode())
            .details(wge.getDetails())
            .fieldErrors(wge.getFieldErrorList())
            .build();


    try {
      setHttpResponseHeaders(response, wge);
      response.getWriter().write(objectMapper.writeValueAsString(errorResult));
    } catch (IOException ioe) {
      // Should not happen
      logger.error("Error setting http response {} ", rte.getMessage());
    }
  }

  /**
   * Set the response header. Doing this avoids an exception being displayed in the logs simply for
   * bad credentials. And this happens when any exception is thrown in a OnePerRequesFilter.
   */
  public static void setHttpResponseHeaders(HttpServletResponse response, 
      WmsGlobalException wge) {
    response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
    response.setStatus(wge.getStatus().value());
    response.addHeader(Constants.X_APPLICATION_ERROR_CODE, wge.getErrorCode().getCode());
    response.addHeader(Constants.X_APPLICATION_ERROR_CODE_LEGACY, wge.getErrorCode().getCode());
    response.addHeader(Constants.X_APPLICATION_ERROR_MESSAGE, wge.getErrorCode().getMessage());
    response.addHeader(Constants.X_APPLICATION_ERROR_DESCRIPTION, wge.getErrorCode()
        .getDescription());
  }

}
