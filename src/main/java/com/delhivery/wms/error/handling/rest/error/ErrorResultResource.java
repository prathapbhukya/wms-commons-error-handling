
package com.delhivery.wms.error.handling.rest.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.delhivery.wms.error.handling.rest.exception.WmsGlobalException;

import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The standard Error result return by the REST error when a problem as occurred.
 *
 * @author prathapb
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
public class ErrorResultResource {
  private String id;
  private Boolean liveMode;
  private ErrorResource error;
  private List<Link> links;

  private ErrorResultResource() {
  }

  /**
   * Construct a new instance of the Error Result using the builder.
   *
   * @param builder the builder to create an error result
   * @return the Error Result
   */
  static ErrorResultResource newInstance(final ErrorResultResource.Builder builder) {
    final ErrorResultResource errorResult = new ErrorResultResource();

    errorResult.id = builder.id;
    errorResult.liveMode = getLiveModeOnlyOnFalse();
    final ErrorResource error = new ErrorResource();
    error.setCode(builder.errorCode.getCode());
    error.setMessage(builder.errorCode.getMessage());
    error.setDetails(builder.detailList);
    error.setFieldErrors(builder.fieldErrors);
    errorResult.error = error;
    errorResult.links = builder.selfLinks;

    return errorResult;
  }

  public static ErrorResultResource from(WmsGlobalException wge) {
    List<Link> newLinks = new ArrayList<>();
    if (wge.getSelfLinks() != null) {
      for (org.springframework.hateoas.Link oldLink : wge.getSelfLinks()) {
        newLinks.add(new Link(oldLink.getRel(), oldLink.getHref()));
      }
    }

    return builder().id(wge.getId()).errorCode(wge.getErrorCode())
        .details(wge.getDetails()).fieldErrors(wge.getFieldErrorList()).selfLinks(newLinks).build();
  }

  /**
   * The resource for which we got an error. Normally there would not be ID exception the the case of a payment
   * decline.
   *
   * @return the resource id
   */
  public String getId() {
    return id;
  }

  public Boolean isLiveMode() {

    return liveMode;
  }

  /**
   * Return the specific details regarding the error.
   *
   * @return the error details
   */
  public ErrorResource getError() {
    return error;
  }

  /**
   * Links to the resource that had the error. Normally there would not be a URI except in the case of a payment
   * decline.
   *
   * @return any links that can be looked up to retrieve the resource.
   */
  public List<Link> getLinks() {
    return links;
  }

  /**
   * Builder to create an ErrorResult.
   *
   * @return ErrorResult.Builder
   */
  public static Builder builder() {
    return new ErrorResultResource.Builder();
  }

  private static Boolean getLiveModeOnlyOnFalse() {
    //return TestModeHolder.getLiveMode() ? null : false;
    return false;
  }

  /**
   * Builder to create a standard Error Result that should be used for all REST API's when a problem is encountered.
   *
   * To use this class, use the builder to create a error response:
   *
   * [source,java] --- ErrorResult errorResult = ErrorResult .builder() .id("ABCD") .errorCode(errorCode) .details("Bad
   * error occurred, doh!") .details( "you really should do something about it", "like now")
   * .selfLink(https://api.qa.netbanx.com/cardpayments/v1/accounts/1000052407") .build() ---
   *
   * @author prathapb
   */
  public static class Builder {
    String id;
    ErrorCode errorCode;
    List<String> detailList = null;
    List<Link> selfLinks = null;
    List<FieldError> fieldErrors = null;

    /**
     * The resource id for the error.
     *
     * @param id this is optional and should only be set for payment declines
     * @return Builder to optionally set other items if required
     */
    public Builder id(final String id) {
      this.id = id;

      return this;
    }

    /**
     * Sets the error code and message.
     *
     * @param errorCode the error details
     * @return Builder to optionally set other items if required
     */
    public Builder errorCode(final ErrorCode errorCode) {
      this.errorCode = errorCode;

      return this;
    }

    /**
     * Add one or more error details.
     *
     * @param details add one or more details
     * @return Builder to optionally set other items if required
     */
    public Builder details(final String... details) {

      if (details != null && details.length > 0) {
        detailList = new ArrayList<>();
        detailList.addAll(Arrays.asList(details));
      }

      return this;
    }

    /**
     * Create a self link to the resource that has the error.
     *
     * @param links list of links to the resource that can be looked up.
     * @return Builder to optionally set other items if required
     */
    public Builder selfLinks(final List<Link> links) {

      if (links != null && !links.isEmpty()) {
        this.selfLinks = links;
      }

      return this;
    }

    /**
     * The field that has an error.
     *
     * @param fieldError a field that is invalid
     * @return Builder to optionally set other items if required
     */
    public Builder fieldError(final FieldError fieldError) {

      if (fieldError != null) {
        fieldErrors = new ArrayList<>();
        fieldErrors.add(fieldError);
      }

      return this;
    }

    /**
     * A list of fields that are invalid.
     *
     * @param fieldErrors a list of fields that are invalid
     * @return Builder to optionally set other items if required
     */
    public Builder fieldErrors(final List<FieldError> fieldErrors) {

      if (fieldErrors != null && !fieldErrors.isEmpty()) {
        this.fieldErrors = fieldErrors;
      }
      return this;
    }

    /**
     * Constructs a new error result.
     *
     * @return creates the error result
     */
    public ErrorResultResource build() {
      return ErrorResultResource.newInstance(this);
    }

  }

}