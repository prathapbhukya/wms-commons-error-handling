
package com.delhivery.wms.error.handling.rest.error;

/**
 * Required parameters for error codes.
 * 
 * @author prathapb
 *
 */
public interface ErrorCode {

  String getCode();

  String getMessage();

  String getDescription();

}
