
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;
import com.delhivery.wms.error.handling.rest.error.ErrorCode;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Common bad request exception. Use the builder to create instances of this class, it supports the
 * following error codes:
 * 
 * * {@link CommonErrorCode#INVALID_CURRENCY} * {@link CommonErrorCode#INVALID_COUNTRY} *
 * {@link CommonErrorCode#ACCOUNT_NOT_FOUND} * {@link CommonErrorCode#ACCOUNT_DISABLED} *
 * {@link CommonErrorCode#REQUEST_NOT_PARSEABLE} *
 * {@link CommonErrorCode#INVALID_MERCHANT_REFERENCE}
 * 
 * 
 * Usage:
 * 
 * [source,java] -- return BadRequestException .builder() .invalidCurrency() .cause(new
 * NullPointerException("NPE")) .details("bad error"); --
 *
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "An invalid request was submitted.")
public class BadRequestException extends WmsGlobalException {
  private static final long serialVersionUID = 1L;

  /**
   * Constructs a new exception with the specified error code and cause.
   * 
   * Note that the detail message associated with {@code cause} is <i>not</i> automatically
   * incorporated in this runtime exception's detail message.
   * 
   * @param errorCode common error code
   * @param cause the cause (which is saved for later retrieval by the {@link #getCause()} method).
   *          (A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or
   *          unknown.)
   */
  private BadRequestException(ErrorCode errorCode, Throwable cause) {
    super(HttpStatus.BAD_REQUEST, errorCode, cause);
  }

  /**
   * Construct a new instance of the exception with the builder.
   * 
   * @param builder used to create this exception
   * @return BadRequestException
   */
  static BadRequestException newInstance(BadRequestException.Builder builder) {
    BadRequestException badRequestException =
        new BadRequestException(builder.errorCode,
        builder.cause);
    if (ArrayUtils.isNotEmpty(builder.getDetails())) {
      badRequestException.setDetails(builder.getDetails());
    }
    
    if (!builder.getSelfLinks().isEmpty()) {
      badRequestException.setSelfLinks(builder.getSelfLinks());
    }

    return badRequestException;
  }

  /**
   * Builder to create an BadRequestException.
   * 
   * @return BadRequestException.Builder
   */
  public static Builder builder() {
    return new BadRequestException.Builder();
  }

  /**
   * Builder to create an internal error exception with the specific return code.
   * 
   * To use this class, use the builder to set the specific error:
   * 
   * [source,java] -- return BadRequestException .builder() .invalidCurrency() .cause(new
   * NullPointerException("NPE")) .details("Could not parse currency"); --
   * 
   * @author prathapb
   *
   */
  public static class Builder
      extends WmsGlobalException.AbstractBuilder<BadRequestException, Builder> {

    /**
     * Used when the currency is invalid or is not supported by your account.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#INVALID_CURRENCY
     */
    public Builder invalidCurrency() {
      return errorCode(CommonErrorCode.INVALID_CURRENCY);
    }

    /**
     * Used when the requested operation is not supported.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#UNSUPPORTED_OPERATION
     */
    public Builder unsupportedOperation() {
      return errorCode(CommonErrorCode.UNSUPPORTED_OPERATION);
    }

    /**
     * Country code is invalid.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#INVALID_COUNTRY
     */
    public Builder invalidCountry() {
      return errorCode(CommonErrorCode.INVALID_COUNTRY);
    }

    /**
     * The account could not be found.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#ACCOUNT_NOT_FOUND
     */
    public Builder accountNotFound() {
      return errorCode(CommonErrorCode.ACCOUNT_NOT_FOUND);
    }

    /**
     * The account is disabled.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#ACCOUNT_DISABLED
     */
    public Builder accountDisabled() {
      return errorCode(CommonErrorCode.ACCOUNT_DISABLED);
    }

    /**
     * The request is not parseable.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#REQUEST_NOT_PARSEABLE
     */
    public Builder requestNotParsable() {
      return errorCode(CommonErrorCode.REQUEST_NOT_PARSEABLE);
    }

    /**
     * The merchant reference field is invalid.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#INVALID_MERCHANT_REFERENCE
     */
    public Builder invalidMerchantReference() {
      return errorCode(CommonErrorCode.INVALID_MERCHANT_REFERENCE);
    }

    /**
     * The test mode can't be used for the transaction.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#TEST_MODE_NOT_PERMITTED
     */
    public Builder testModeNotPermitted() {

      return errorCode(CommonErrorCode.TEST_MODE_NOT_PERMITTED);
    }

    /**
     * The maximum upload size is exceeded..
     *
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#MAX_UPLOAD_SIZE_EXCEEDED
     */
    public Builder maxUploadSizeExceeded() {
      return errorCode(CommonErrorCode.MAX_UPLOAD_SIZE_EXCEEDED);
    }

    /**
     * Sets the appropriate return code when constructing the exception.
     * 
     * @return a BadRequestException with the correct error code
     */
    @Override
    public BadRequestException build() {
      return newInstance(this);
    }

  }

}
