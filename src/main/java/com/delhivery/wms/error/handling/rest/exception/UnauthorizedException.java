
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;
import com.delhivery.wms.error.handling.rest.error.ErrorCode;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Common unauthorized exception. Use the builder to create instances of this class, it supports the
 * following error codes:
 * 
 * * {@link CommonErrorCode#CREDENTIALS_EXPIRED} * {@link CommonErrorCode#CREDENTIALS_DISABLED} *
 * {@link CommonErrorCode#CREDENTIALS_LOCKED} * {@link CommonErrorCode#CREDENTIALS_INVALID} *
 * {@link CommonErrorCode#CREDENTIALS_UNEXPECTED_ERROR}
 * 
 * 
 * Usage:
 * 
 * [source,java] -- return InternalErrorException .builder() .invalidCurrency() .cause(new
 * NullPointerException("NPE")) .details("bad error"); --
 * 
 * 
 * @author prathapb
 * 
 *
 */
@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "Not authorized to access this system. "
    + "Could be due to invalid or missing credentials. ")
public class UnauthorizedException extends WmsGlobalException {
  private static final long serialVersionUID = 1L;

  /**
   * Constructs a new exception with the specified error code and cause.
   * 
   * Note that the detail message associated with {@code cause} is <i>not</i> automatically
   * incorporated in this runtime exception's detail message.
   * 
   * @param errorCode common error code
   * @param cause the cause (which is saved for later retrieval by the {@link #getCause()} method).
   *          (A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or
   *          unknown.)
   */
  private UnauthorizedException(ErrorCode errorCode, Throwable cause) {
    super(HttpStatus.UNAUTHORIZED, errorCode, cause);
  }

  /**
   * Construct a new instance of the exception with the builder.
   * 
   * @param builder used to create this exception
   * @return BadRequestException
   */
  static UnauthorizedException newInstance(UnauthorizedException.Builder builder) {
    UnauthorizedException unauthorizedException =
        new UnauthorizedException(builder.errorCode,
        builder.cause);
    if (ArrayUtils.isNotEmpty(builder.getDetails())) {
      unauthorizedException.setDetails(builder.getDetails());
    }
    return unauthorizedException;
  }

  /**
   * Builder to create an UnauthorizedException.
   * 
   * @return UnauthorizedException.Builder
   */
  public static Builder builder() {
    return new UnauthorizedException.Builder();
  }

  /**
   * Builder to create an internal error exception with the specific return code.
   * 
   * To use this class, use the builder to set the specific error:
   * 
   * [source,java] -- return UnauthorizedException .builder() .expired() .cause(new
   * NullPointerException("NPE")) .details("Could not parse currency"); --
   * 
   * @author prathapb
   *
   */
  public static class Builder
      extends WmsGlobalException.AbstractBuilder<UnauthorizedException, Builder> {

    /**
     * The authentication credentials have expired.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#CREDENTIALS_EXPIRED
     */
    public Builder expired() {
      return errorCode(CommonErrorCode.CREDENTIALS_EXPIRED);
    }

    /**
     * The authentication credentials have been disabled.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#CREDENTIALS_DISABLED
     */
    public Builder disabled() {
      return errorCode(CommonErrorCode.CREDENTIALS_DISABLED);
    }

    /**
     * The authentication credentials have been locked.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#CREDENTIALS_LOCKED
     */
    public Builder locked() {
      return errorCode(CommonErrorCode.CREDENTIALS_LOCKED);
    }

    /**
     * The credentials cannot be authenticated for unexpected reasons.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#CREDENTIALS_UNEXPECTED_ERROR
     */
    public Builder unknown() {
      return errorCode(CommonErrorCode.ACCOUNT_DISABLED);
    }

    /**
     * The authentication credentials are invalid.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#CREDENTIALS_INVALID
     */
    public Builder requestNotParsable() {
      return errorCode(CommonErrorCode.CREDENTIALS_INVALID);
    }

    /**
     * The authentication credentials are invalid.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#CREDENTIALS_INVALID
     */
    public Builder unauthorized() {
      return errorCode(CommonErrorCode.UNAUTHORIZED_ACCESS);
    }

    /**
     * Sets the appropriate return code when constructing the exception.
     * 
     * @return a BadRequestException with the correct error code
     */
    @Override
    public UnauthorizedException build() {
      return newInstance(this);
    }

  }

}
