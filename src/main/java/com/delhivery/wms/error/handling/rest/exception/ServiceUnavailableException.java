
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;

import org.springframework.http.HttpStatus;

public class ServiceUnavailableException extends WmsGlobalException {

  private static final long serialVersionUID = -5353093690359737455L;

  public ServiceUnavailableException() {
    super(HttpStatus.SERVICE_UNAVAILABLE, CommonErrorCode.SERVICE_UNAVAILABLE);
  }


}
