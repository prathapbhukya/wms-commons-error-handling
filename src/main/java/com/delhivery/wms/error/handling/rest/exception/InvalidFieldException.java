
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

/**
 * Common invalid field exception.
 * 
 * It supports the {@link CommmonErrorCode#INVALID_FIELD} code;
 * 
 * @author prathapb
 *
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "An invalid or missing field was found.")
public class InvalidFieldException extends WmsGlobalException {

  private static final long serialVersionUID = 1L;

  public InvalidFieldException(List<FieldError> fieldErrorList) {
    this(fieldErrorList, null);
  }

  public InvalidFieldException(List<FieldError> fieldErrorList, Throwable cause) {
    super(HttpStatus.BAD_REQUEST, CommonErrorCode.INVALID_FIELD, fieldErrorList, cause);
  }

}
