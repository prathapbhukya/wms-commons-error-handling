
package com.delhivery.wms.error.handling.rest.error;

/**
 * Common Link Types.
 * 
 * @author prathapb
 *
 */
public enum LinkType {
  SELF("self"), ERROR_INFO("errorinfo");

  String value;

  private LinkType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

}
