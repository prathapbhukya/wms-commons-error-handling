
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;
import com.delhivery.wms.error.handling.rest.error.ErrorCode;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Common conflict request exception.Use the builder to create instances of this class, it supports
 * the following error codes:
 * 
 * * {@link CommonErrorCode#DUPLICATE_MERCHANT_REFERENCE_NUMBER}
 * 
 * @author prathapb
 * 
 */
@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Resource already exits.")
public class ResourceAlreadyExistsException extends WmsGlobalException {

  private static final long serialVersionUID = 1L;

  public ResourceAlreadyExistsException(final ErrorCode errorCode, final Throwable cause) {
    super(HttpStatus.CONFLICT, errorCode, cause);
  }

  /**
   * Construct a new instance of the exception with the builder.
   * 
   * @param builder used to create this exception
   * @return ResourceAlreadyExistsException
   */
  static ResourceAlreadyExistsException
      newInstance(final ResourceAlreadyExistsException.Builder builder) {
    final ResourceAlreadyExistsException resourceAlreadyExistsException =
        new ResourceAlreadyExistsException(builder.errorCode, builder.cause);
    if (ArrayUtils.isNotEmpty(builder.getDetails())) {
      resourceAlreadyExistsException.setDetails(builder.getDetails());
    }
    return resourceAlreadyExistsException;
  }

  /**
   * Builder to create an ResourceAlreadyExistsException.
   * 
   * @return ResourceAlreadyExistsException.Builder
   */
  public static Builder builder() {
    return new ResourceAlreadyExistsException.Builder();
  }

  /**
   * Builder to create an ResourceAlreadyExistsException exception with the specific return code.
   * 
   * Example:
   * 
   * [source,java] -- return ResourceAlreadyExistsException.builder()
   *
   */
  public static class Builder
      extends WmsGlobalException.AbstractBuilder<ResourceAlreadyExistsException, Builder> {

    /**
     * The merchant reference number has already been used.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#DUPLICATE_MERCHANT_REFERENCE_NUMBER
     */
    public Builder duplicateMerchantReferenceNumber() {

      return errorCode(CommonErrorCode.DUPLICATE_MERCHANT_REFERENCE_NUMBER);
    }

    /**
     * Sets the appropriate return code when constructing the exception.
     * 
     * @return a ResourceAlreadyExistsException with the correct error code
     */
    @Override
    public ResourceAlreadyExistsException build() {
      return newInstance(this);
    }

  }

}
