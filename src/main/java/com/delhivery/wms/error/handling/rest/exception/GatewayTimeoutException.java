
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;

import org.springframework.http.HttpStatus;

public class GatewayTimeoutException extends WmsGlobalException {

  private static final long serialVersionUID = 7678062188609854248L;

  public GatewayTimeoutException() {
    super(HttpStatus.GATEWAY_TIMEOUT, CommonErrorCode.GATEWAY_TIMEOUT);
  }

}
