
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Common unauthorized access exception.
 *
 */
@ResponseStatus(value = HttpStatus.FORBIDDEN,
    reason = "The credentials were valid but are not " + "the server refused to fulfill it.")
public class UnauthorizedAccessException extends WmsGlobalException {
  private static final long serialVersionUID = 1L;

  /**
   * Constructs a new exception. The cause is not initialized, and may subsequently be initialized
   * by a call to {@link #initCause}.
   * 
   * @see CommonErrorCode#UNAUTHORIZED_ACCESS
   * 
   */
  public UnauthorizedAccessException() {
    super(HttpStatus.FORBIDDEN, CommonErrorCode.UNAUTHORIZED_ACCESS);
  }
}
