
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.ErrorCode;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This should be thrown or extended when the application declines a request. To create application
 * specific errors. you should implement the {@link #ErrorCode} interface and extend this class with
 * your exception.
 * 
 * @author prathapb
 *
 */
@ResponseStatus(value = HttpStatus.PAYMENT_REQUIRED,
    reason = "The parameters in your request are valid but the application delined your request.")
public class ApplicationProcessingException extends WmsGlobalException {
  private static final long serialVersionUID = 1L;

  /**
   * Constructs a new exception. The cause is not initialized, and may subsequently be initialized
   * by a call to {@link #initCause}.
   * 
   * 
   */
  public ApplicationProcessingException(ErrorCode errorCode) {
    this(errorCode, null);
  }

  /**
   * Constructs a new exception and cause.
   * 
   * Note that the detail message associated with {@code cause} is <i>not</i> automatically
   * incorporated in this runtime exception's detail message.
   * 
   * @param cause the cause (which is saved for later retrieval by the {@link #getCause()} method).
   *          (A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or
   *          unknown.)
   */
  public ApplicationProcessingException(ErrorCode errorCode, Throwable cause) {
    super(HttpStatus.PAYMENT_REQUIRED, errorCode, cause);
  }

}
