
package com.delhivery.wms.error.handling.rest.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
public class FieldErrorResource implements java.io.Serializable {
  private static final long serialVersionUID = -4397634118277601623L;

  private String field;

  private String error;

  public String getField() {
    return field;
  }

  public void setField(String field) {
    this.field = field;
  }

  public String getError() {
    return error;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("FieldError [field=");
    builder.append(field);
    builder.append(", error=");
    builder.append(error);
    builder.append("]");
    return builder.toString();
  }

  public void setError(String error) {
    this.error = error;
  }

}
