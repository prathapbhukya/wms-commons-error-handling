
package com.delhivery.wms.error.handling.rest.error;

import org.apache.commons.lang3.builder.ToStringBuilder;

// @formatter:off

/**
 * = Common error codes.
 *
 * == Internal Errors
 * * {@link #INTERNAL_ERROR}
 * * {@link #DATABASE_ERROR}
 * * {@link #CRYPTOGRAPHIC_ERROR}
 * * {@link #GATEWAY_ERROR}
 *
 * == Bad Request
 * * {@link #INVALID_CURRENCY}
 * * {@link #INVALID_COUNTRY}
 * * {@link #ACCOUNT_NOT_FOUND}
 * * {@link #ACCOUNT_DISABLED}
 * * {@link #REQUEST_NOT_PARSEABLE}
 * * {@link #INVALID_MERCHANT_REFERENCE}
 * * {@link #INVALID_FIELD}
 *
 * == Conflict
 * * {@link #DUPLICATE_ENTITY}
 * * {@link #STATE_CHANGE}
 *
 * == Unauthorized
 * * {@link #CREDENTIALS_EXPIRED}
 * * {@link #CREDENTIALS_DISABLED}
 * * {@link #CREDENTIALS_LOCKED}
 * * {@link #CREDENTIALS_UNEXPECTED_ERROR}
 * * {@link #CREDENTIALS_INVALID}
 *
 * == Unauthorized Access
 * * {@link #UNAUTHORIZED_ACCESS}
 *
 * == Not Found
 * * {@link #ENTITY_NOT_FOUND}
 * * {@link #URI_NOT_FOUND}
 * * {@link #CARD_NOT_FOUND}
 * * {@link #PAYLOAD_CONFIG_NOT_FOUND}
 *
 * == {@link #UNSUPPORTED_RESPONSE_FORMAT}
 * == {@link #UNSUPPORTED_CONTENT_TYPE}
 * == {@link #TOO_MANY_REQUESTS}
 */
public enum CommonErrorCode implements ErrorCode {

  /**
   * == 1000 : An internal error occurred.
   * * HTTP status: 500
   * * code: 1000
   * * Message: Internal Error
   */
  INTERNAL_ERROR("1000", "Internal Error", "An internal error occurred."),

  /**
   * == 1001 : An external gateway error occurred.
   * * HTTP status: 502
   * * code: 1001
   * * Message: External Gateway Error
   */
  EXTERNAL_GATEWAY_ERROR("1001", "External Gateway Error", "An external gateway error occurred."),

  /**
   * == 1002 : An internal database error occurred.
   * * HTTP status: 500
   * * code: 1002
   * * Message: Database Error
   */
  DATABASE_ERROR("1002", "Database Error", "An internal database error occurred."),

  /**
   * == 1003 : Error calling cryptographic function like trying to encrypt or decrypt.
   * * HTTP status: 500
   * * code: 1003
   * * Message: Cryptographic Error
   */
  CRYPTOGRAPHIC_ERROR("1003", "Cryptographic Error", "Error while trying to encrypt or decrypt."),

  /**
   * == 1007 : Error communicating with downstream provider.
   * * HTTP status: 500
   * * code: 1007
   * * Message: Gateway Error
   */
  GATEWAY_ERROR("1007", "Gateway Error", "Error communicating with downstream provider."),

  /**
   * == 1010 : Service unavailable.
   *
   * * HTTP status: 503
   * * code: 1010
   * * Message: Service unavailable
   */
  SERVICE_UNAVAILABLE("1010", "Service unavailable", "The service is currrently unavailable"),

  /**
   * == 1020 : Gateway timeout.
   *
   * * HTTP status: 504
   * * code: 1020
   * * Message: Gateway timeout
   */
  GATEWAY_TIMEOUT("1020", "Gateway timeout", "The server did not respond in a timely fashion"),

  /**
   * == 5001 : The currency code is invalid or your account does not support this currency.
   * * HTTP status: 400
   * * code: 5001
   * * Message: Invalid currency
   */
  INVALID_CURRENCY("5001", "Invalid currency",
      "The currency code is invalid or your account does not support this currency."),

  /**
   * == 5005 : You submitted an an invalid or unsupported operation type with your request.
   * * HTTP status: 400
   * * code: 5005
   * * Message: Unsupported operation
   */
  UNSUPPORTED_OPERATION("5005", "Unsupported operation",
      "You submitted an an invalid or unsupported operation type with your request."),

  /**
   * == 5010 : The submitted country code is invalid.
   * * HTTP status: 400
   * * code: 5010
   * * Message: Invalid country
   */
  INVALID_COUNTRY("5010", "Invalid country", "The submitted country code is invalid."),

  /**
   * == 5016 : The account provided cannot be found.
   * * HTTP status: 400
   * * code: 5016
   * * Message: Account not found
   */
  ACCOUNT_NOT_FOUND("5016", "Account not found", "The account provided cannot be found."),

  /**
   * == 5017 : The account provided is disabled.
   * * HTTP status: 400
   * * code: 5017
   * * Message: Account disabled
   */
  ACCOUNT_DISABLED("5017", "Account disabled", "The account provided is disabled."),

  /**
   * == 5023 : The request is not parseable.
   * * HTTP status: 400
   * * code: 5023
   * * Message: Request body not parsable
   */
  REQUEST_NOT_PARSEABLE("5023", "Request body not parsable", "The request is not parseable."),

  /**
   * == 5042 : The merchant reference field is invalid.
   * * HTTP status: 400
   * * code: 5042
   * * Message: Invalid merchant reference
   */
  INVALID_MERCHANT_REFERENCE("5042", "Invalid merchant reference",
    "The merchant reference field is invalid"),

  /**
   * == 5068 : Missing a mandatory field or the value of a field does not match the format expected.
   * * HTTP status: 400
   * * code: 5068
   * * Message: Field error(s)
   */
  INVALID_FIELD("5068", "Field error(s)", "Either you submitted a request that is "
      + "missing a mandatory field or the value of a field does not match the format expected."),

  /**
   * == 5282 : The request sent with test mode is not allowed.
   * * HTTP status: 400
   * * code: 5282
   * * Message: Test mode not allowed
   */
  TEST_MODE_NOT_PERMITTED(
      "5282",
      "Mode not permitted",
      "The request was sent in a mode that is not permitted. For example, a Live refund "
          + "request was made on a Test authorization request."),

  /**
   * == 5282 : The maximum upload size is exceeded.
   * * HTTP status: 400
   * * code: 5382
   * * Message: Maximum upload size exceeded
   */
  MAX_UPLOAD_SIZE_EXCEEDED("5282", "Maximum upload size exceeded", "The maximum upload size is exceeded"),

  /**
   * == 5031 : The provided reference has already been used for another request.
   * * HTTP status: 409
   * * code: 5031
   * * Message: The provided reference has already been used for another request.
   */
  DUPLICATE_MERCHANT_REFERENCE_NUMBER("5031", "Duplicate merchant reference",
      "The provided reference has already been used for another request."),

  /**
   * == 5275 : The authentication credentials have expired.
   * * HTTP status: 401
   * * code: 5275
   * * Message: Authentication credentials expired
   */
  CREDENTIALS_EXPIRED("5275", "Authentication credentials expired",
    "The authentication credentials have expired."),

  /**
   * == 5276 : The authentication credentials have been disabled.
   *
   * * HTTP status: 401
   * * code: 5276
   * * Message: Authentication credentials disabled
   */
  CREDENTIALS_DISABLED("5276", "Authentication credentials disabled",
    "The authentication credentials have been disabled."),

  /**
   * == 5277 : The authentication credentials have been locked.
   *
   * * HTTP status: 401
   * * code: 5277
   * * Message: Authentication credentials locked
   */
  CREDENTIALS_LOCKED("5277", "Authentication credentials locked",
    "The authentication credentials have been locked."),

  /**
   * == 5278 : The authentication credentials have been locked.
   *
   * * HTTP status: 401
   * * code: 5278
   * * Message: Cannot authenticate
   */
  CREDENTIALS_UNEXPECTED_ERROR("5278", "Cannot authenticate",
    "The credentials cannot be authenticated for unexpected reasons."),

  /**
   * == 5279 : The authentication credentials are invalid.
   *
   * * HTTP status: 401
   * * code: 5279
   * * Message: Authentication credentials are invalid
   */
  CREDENTIALS_INVALID("5279", "Invalid credentials", "The authentication credentials are invalid."),

  /**
   * == 5270 : The credentials do not have permission to access the requested data.
   *
   * * HTTP status: 403
   * * code: 5270
   * * Message: Unauthorized access
   */
  UNAUTHORIZED_ACCESS("5270", "Unauthorized access",
    "The credentials do not have permission to access the requested data."),

  /**
   * == 5269 : The ID(s) specified in the URL do not correspond to the values in the system.
   *
   * * HTTP status: 404
   * * code: 5269
   * * Message: Entity not found
   */
  ENTITY_NOT_FOUND("5269", "Entity not found",
    "The ID(s) specified in the URL do not correspond to the values in the system."),

  /**
   * == 5273 : Your client reached our application but the URL is invalid.
   *
   * * HTTP status: 404
   * * code: 5273
   * * Message: URI not found
   */
  URI_NOT_FOUND("5273", "URI not found",
    "Your client reached our application but we were unable to service your request due to an "
        + "invalid URL."),

  /**
   * == 5271 : You requested a response in the 'Accept' header that is in an unsupported format.
   *
   * * HTTP status: 406
   * * code: 5271
   * * Message: Unsupported 'Accept' header
   */
  UNSUPPORTED_RESPONSE_FORMAT("5271", "Unsupported 'Accept' header",
    "You requested a response in the 'Accept' header that is in an unsupported format."),

  /**
   * == 5272 : The 'Content-Type' in the request header is an unsupported format.
   *
   * * HTTP status: 415
   * * code: 5272
   * * Message: Unauthorized access
   */
  UNSUPPORTED_CONTENT_TYPE("5272", "Unsupported 'Content-Type'",
    "The 'Content-Type' in the request header is an unsupported format."),

  /**
   * == 5281 : The server does not support the functionality required to fulfill the request.
   *
   * * HTTP status: 405
   * * code: 5281
   * * Message: Not supported
   */
  NOT_SUPPORTED("5281", "Not supported",
      "The server does not support the request method for the given resource."),

  /**
   * == 1200 : The user has sent too many requests in a given amount of time.
   *
   * * HTTP status: 429
   * * code: 1200
   * * Message: API call rate exceeded
   */
  TOO_MANY_REQUESTS("1200", "API call rate exceeded",
    "The user has sent too many requests in a given amount of time."),

  /**
   * == 5284 : An entity with the specified identifier(s) already exists.
   * * HTTP status: 409
   * * code: 5284
   * * Message: An entity with the specified identifier(s) already exists.
   */
  DUPLICATE_ENTITY("5284", "Duplicate entity",
      "An entity with the specified identifier(s) already exists."),

  /**
   * == 5285 : Requested state change conflicts with existing state of resource.
   * * HTTP status: 409
   * * code: 5285
   * * Message: Requested state change conflicts with existing state of resource.
   */
  STATE_CHANGE("5285", "State change conflict",
      "Requested state change conflicts with existing state of resource."),

  /**
   * == 5286 : Card with the specified identifier(s) does not exists.
   *
   * * HTTP status: 404
   * * code: 5286
   * * Message: Card not found
   */
  CARD_NOT_FOUND("5286", "Card not found",
      "Card with the specified identifier(s) does not exists."),

  /**
   * == 5287 : Payload config with the specified identifier(s) does not exists.
   *
   * * HTTP status: 404
   * * code: 5287
   * * Message: Card not found
   */
  PAYLOAD_CONFIG_NOT_FOUND("5287", "Payload config not found",
      "Payload config with the specified identifier(s) does not exists."),

  /**
   * == 5288 : Card does not belong to consumer.
   *
   * * HTTP status: 404
   * * code: 5288
   * * Message: Card does not belong to consumer
   */
  CARD_DOES_NOT_BELONG_TO_CONSUMER("5288", "Card does not belong to consumer",
      "Card belongs to a different consumer than the given one."),

  /**
   * == 5301 : Cards max number exceeded.
   *
   * * HTTP status: 409
   * * code: 5301
   * * Message: Cards max number exceeded
   */
  CARDS_MAX_NUMBER_EXCEEDED("5301", "Cards max number exceeded",
      "Cards max number exceeded."),

  /**
   * == 5302 : Stamp expired.
   *
   * * HTTP status: 400
   * * code: 5302
   * * Message: Stamp expired
   */
  STAMP_EXPIRED("5302", "Stamp expired", "Stamp expired"),

  /**
   * == 5303 : Payment instrument already exists.
   *
   * * HTTP status: 409
   * * code: 5303
   * * Message: Payment instrument already exists.
   */
  PAYMENT_INSTRUMENT_ALREADY_EXISTS("5303","Payment instrument already exists","Payment instrument already exists"),

  /**
   * == 5303 : Card expired.
   *
   * * HTTP status: 400
   * * code: 5303
   * * Message: Card expired.
   */
  EXPIRED_CARD("5304", "Expired card", "Card expired"),
;
  //@formatter:on

  private final String code;
  private final String message;
  private final String description;

  /**
   * Construct an error code with a external message and internal description.
   *
   * @param code common error code
   * @param message external, customer friendly message
   * @param description internal description
   */
  CommonErrorCode(String code, String message, String description) {
    this.code = code;
    this.message = message;
    this.description = description;
  }

  @Override
  public String getCode() {
    return this.code;
  }

  @Override
  public String getMessage() {
    return this.message;
  }

  @Override
  public String getDescription() {
    return this.description;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}