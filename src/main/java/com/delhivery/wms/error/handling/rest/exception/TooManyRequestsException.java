
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Common too many requests exception.
 *
 */
@ResponseStatus(value = HttpStatus.TOO_MANY_REQUESTS,
    reason = "The user has sent too many requests in a given amount of time.")
public class TooManyRequestsException extends WmsGlobalException {
  private static final long serialVersionUID = 1L;

  /**
   * Constructs a new exception. The cause is not initialized, and may subsequently be initialized
   * by a call to {@link #initCause}.
   * 
   * @see CommonErrorCode#TOO_MANY_REQUESTS
   * 
   */
  public TooManyRequestsException() {
    super(HttpStatus.TOO_MANY_REQUESTS, CommonErrorCode.TOO_MANY_REQUESTS);
  }
}
