
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Common unsupported content type exception.
 *
 */
@ResponseStatus(value = HttpStatus.UNSUPPORTED_MEDIA_TYPE, reason = "The 'Content-Type' you "
    + "specified in request header was submitted in an unsupported format.")
public class UnsupportedContentTypeException extends WmsGlobalException {
  private static final long serialVersionUID = 1L;

  /**
   * Constructs a new exception. The cause is not initialized, and may subsequently be initialized
   * by a call to {@link #initCause}.
   * 
   * @see CommonErrorCode#UNSUPPORTED_CONTENT_TYPE
   * 
   */
  public UnsupportedContentTypeException() {
    super(HttpStatus.UNSUPPORTED_MEDIA_TYPE, CommonErrorCode.UNSUPPORTED_CONTENT_TYPE);
  }
}
