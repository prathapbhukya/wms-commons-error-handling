
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Not supported exception.
 * 
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND,
    reason = "The server does not support the functionality required to fulfill the request.")
public class NotSupportedException extends WmsGlobalException {

  private static final long serialVersionUID = 1L;

  /**
   * Constructs a new exception. The cause is not initialized, and may subsequently be initialized
   * by a call to {@link #initCause}.
   * 
   * @see CommonErrorCode#TOO_MANY_REQUESTS
   * 
   */
  public NotSupportedException() {

    super(HttpStatus.METHOD_NOT_ALLOWED, CommonErrorCode.NOT_SUPPORTED);
  }
}
