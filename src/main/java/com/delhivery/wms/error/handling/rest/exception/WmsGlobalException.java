
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;
import com.delhivery.wms.error.handling.rest.error.ErrorCode;

import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Root of the hierarchy of one platform exceptions. As this class is a runtime exception, there is
 * no need for user code to catch it.
 *
 * @author prathapb
 */
public abstract class WmsGlobalException extends RuntimeException {
  static final String[] EMPTY = new String[0];
  private static final long serialVersionUID = 1L;

  private String id;
  private HttpStatus status;
  private ErrorCode errorCode;
  private List<String> details;
  private List<FieldError> fieldErrorList;
  private List<Link> selfLinks;

  /**
   * Constructs a new runtime exception with the specified detail message. The cause is not
   * initialized, and may subsequently be initialized by a call to {@link #initCause}.
   *
   * @param status the HTTP status error code
   * @param errorCode the wms gloabl exception error code
   */
  public WmsGlobalException(HttpStatus status, ErrorCode errorCode) {
    this(status, errorCode, null);
  }

  /**
   * Constructs a new runtime exception with the specified detail message and cause.
   * <p>
   * Note that the detail message associated with {@code cause} is <i>not</i> automatically
   * incorporated in this runtime exception's detail message.
   */
  public WmsGlobalException(HttpStatus status, ErrorCode errorCode, Throwable cause) {
    super(errorCode == null
            ? CommonErrorCode.INTERNAL_ERROR.getMessage()
            : errorCode.getMessage(),
        cause);

    ErrorCode code = errorCode == null ? CommonErrorCode.INTERNAL_ERROR : errorCode;

    this.errorCode = code;
    this.status = status;
    details = new ArrayList<>();
    if (code.getDescription() != null) {
      details.add(code.getDescription());
    }
    fieldErrorList = new ArrayList<>();
  }

  /**
   * Constructs a new runtime exception with the specified detail message. The cause is not
   * initialized, and may subsequently be initialized by a call to {@link #initCause}.
   */
  public WmsGlobalException(HttpStatus status, List<FieldError> fieldErrorList,
      ErrorCode errorCode) {
    this(status, errorCode, fieldErrorList, null);
  }

  /**
   * Constructs a new runtime exception with the specified detail message and cause.
   * <p>
   * Note that the detail message associated with {@code cause} is <i>not</i> automatically
   * incorporated in this runtime exception's detail message.
   */
  public WmsGlobalException(HttpStatus status, ErrorCode errorCode,
      List<FieldError> fieldErrorList, Throwable cause) {
    super(errorCode == null
            ? CommonErrorCode.INTERNAL_ERROR.getMessage()
            : errorCode.getMessage(),
        cause);
    ErrorCode code = errorCode == null ? CommonErrorCode.INTERNAL_ERROR : errorCode;
    this.errorCode = code;
    this.status = status;
    details = new ArrayList<>();
    if (code.getDescription() != null) {
      details.add(code.getDescription());
    }
    this.fieldErrorList = fieldErrorList;
  }

  public String getId() {

    return id;
  }

  public void setId(String id) {

    this.id = id;
  }

  /**
   * The HTTP status of this error.
   *
   * @return the http status of this error
   */
  public HttpStatus getStatus() {
    return this.status;
  }

  /**
   * The error code.
   *
   * @return the error code
   */
  public ErrorCode getErrorCode() {
    return this.errorCode;
  }

  /**
   * The detailed message.
   *
   * @return detailed message regarding this exception
   */
  public String[] getDetails() {
    return details == null ? EMPTY : details.toArray(new String[details.size()]);
  }

  /**
   * The detailed message.
   *
   * @param details detailed message regarding this exception
   */
  public void setDetails(String... details) {
    if (details != null) {
      this.details = Arrays.asList(details);
    }
  }

  /**
   * The list of fields with errors.
   *
   * @return the fieldErrorList
   */
  public List<FieldError> getFieldErrorList() {
    return fieldErrorList;
  }

  /**
   * The list of fields with errors.
   *
   * @param fieldErrorList the fieldErrorList to set
   */
  public void setFieldErrorList(List<FieldError> fieldErrorList) {
    if (fieldErrorList != null) {
      this.fieldErrorList = fieldErrorList;
    }
  }

  public List<Link> getSelfLinks() {

    return selfLinks;
  }

  public void setSelfLinks(Link... selfLinks) {
    if (selfLinks != null) {
      this.selfLinks = Arrays.asList(selfLinks);
    }
  }

  public void setSelfLinks(List<Link> selfLinks) {
    if (selfLinks != null) {
      this.selfLinks = selfLinks;
    }
  }

  /**
   * @param selfLink adds single Link to the array list of Link
   * @deprecated Please use the array of Link setSelfLinks. Replaced by {@link #setSelfLinks}
   */
  @Deprecated
  public void setSelfLink(Link selfLink) {
    if (selfLink != null) {
      this.setSelfLinks(selfLink);
    }
  }

  /**
   * @deprecated Please use the array of Link getSelfLinks. Replaced by {@link #getSelfLinks}
   * @return the first element of the selfLinks array
   */
  @Deprecated
  public Link getSelfLink() {
    if (!getSelfLinks().isEmpty()) {
      return getSelfLinks().get(0);
    }
    return null;
  }

  @SuppressWarnings("rawtypes")
  // Eclipse formatter bug: https://bugs.eclipse.org/bugs/show_bug.cgi?id=384959
  // @formatter:off
  public abstract static class AbstractBuilder
      <T extends WmsGlobalException, B extends AbstractBuilder> {
 // @formatter:on
    protected ErrorCode errorCode;
    protected Throwable cause;
    protected List<Link> selfLinks;
    protected List<String> details = new ArrayList<>();

    public B cause(Throwable cause) {
      this.cause = cause;
      return returnBuilder();
    }

    public Throwable getCause() {
      return this.cause;
    }

    /**
     * Creates a list of details to provide additional information about the error condition.
     *
     * @param details one or many details
     * @return the builder
     */
    public B details(String... details) {
      if (details != null) {
        this.details = Arrays.asList(details);
      }
      return returnBuilder();
    }

    /**
     * Creates a localized formatted string, using the supplied format and arguments, using the
     * user's default locale and sets it as part of the detailed message. This should be used to
     * provide additional help in resolving the issue.
     *
     * @param format the format string (see {@link java.util.Formatter#format})
     * @param args the list of arguments passed to the formatter. If there are more arguments than required by {@code
     * format} then the additional arguments are ignored.
     * @return the builder
     * @throws NullPointerException if {@code format} is null
     */
    public B detail(String format, Object... args) {
      String formattedString = String.format(format, args);
      details.add(formattedString);
      return returnBuilder();
    }

    public String[] getDetails() {
      return details == null ? EMPTY : details.toArray(new String[details.size()]);
    }

    public B errorCode(ErrorCode errorCode) {
      this.errorCode = errorCode;
      return returnBuilder();
    }

    public B selfLinks(Link... selfLinks) {
      if (selfLinks != null) {
        this.selfLinks = Arrays.asList(selfLinks);
      }
      return returnBuilder();
    }

    public List<Link> getSelfLinks() {
      return selfLinks == null ? Collections.emptyList() : selfLinks;
    }

    protected ErrorCode getErrorCode() {
      return this.errorCode;
    }

    public abstract T build();

    @SuppressWarnings("unchecked")
    protected B returnBuilder() {
      return (B) this;
    }

  }

}
