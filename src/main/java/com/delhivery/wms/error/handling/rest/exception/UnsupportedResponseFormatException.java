
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Common unsupported response type exception.
 *
 */
@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, reason = "You requested a response in the "
    + "'Accept' header that is in an unsupported format.")
public class UnsupportedResponseFormatException extends WmsGlobalException {
  private static final long serialVersionUID = 1L;

  /**
   * Constructs a new exception. The cause is not initialized, and may subsequently be initialized
   * by a call to {@link #initCause}.
   * 
   * @see CommonErrorCode#UNSUPPORTED_RESPONSE_FORMAT
   * 
   */
  public UnsupportedResponseFormatException() {
    super(HttpStatus.NOT_ACCEPTABLE, CommonErrorCode.UNSUPPORTED_RESPONSE_FORMAT);
  }
}
