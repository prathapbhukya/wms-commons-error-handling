
package com.delhivery.wms.error.handling.rest.error;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * A link the resources that can be looked up by the client.
 * 
 * @author prathapb
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
class Link {

  protected String rel;
  protected String href;

  Link() {
    // Make this package scope, should be created with the builder
  }

  /**
   * Creates a new link to the given URI with the given rel.
   * 
   * @param rel the relation the link represents
   * @param href the URI
   */
  Link(String rel, String href) {
    this.rel = rel;
    this.href = href;
  }

  /**
   * Creates a new link to the given URI with the given LinkType.
   * 
   * @param rel the relation the link represents
   * @param href the URI
   */
  Link(LinkType linkType, String href) {
    this(linkType.getValue(), href);
  }

  /**
   * The Rel of the link.
   * 
   * @return the relation this link represents
   */
  public String getRel() {
    return rel;
  }

  /**
   * The relation this link represents.
   * 
   * @param rel the relation
   */
  public void setRel(String rel) {
    this.rel = rel;
  }

  /**
   * The actual URI this link is pointing to.
   * 
   * @return the URI for this link
   */
  public String getHref() {
    return href;
  }

  /**
   * The actual URI this link points to.
   * 
   * @param href the URI for this link
   */
  public void setHref(String href) {
    this.href = href;
  }

}
