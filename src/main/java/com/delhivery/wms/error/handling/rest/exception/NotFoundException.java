
package com.delhivery.wms.error.handling.rest.exception;

import com.delhivery.wms.error.handling.rest.error.CommonErrorCode;
import com.delhivery.wms.error.handling.rest.error.ErrorCode;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Common resource not found exception. Use the builder to create instances of this class, it
 * supports the following error codes:
 * 
 * * {@link CommonErrorCode#ENTITY_NOT_FOUND} * {@link CommonErrorCode#URI_NOT_FOUND}
 * 
 * Usage:
 * 
 * [source,java] -- return NotFoundException .builder() .entityNotFound() .cause(new
 * NullPointerException("NPE")) .details("bad error"); --
 *
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND,
    reason = "Could not find resource specfied in request URI.")
public class NotFoundException extends WmsGlobalException {
  private static final long serialVersionUID = 1L;

  /**
   * Constructs a new exception with the specified error code and cause.
   * 
   * Note that the detail message associated with {@code cause} is <i>not</i> automatically
   * incorporated in this runtime exception's detail message.
   * 
   * @param errorCode common error code
   * @param cause the cause (which is saved for later retrieval by the {@link #getCause()} method).
   *          (A <tt>null</tt> value is permitted, and indicates that the cause is nonexistent or
   *          unknown.)
   */
  private NotFoundException(ErrorCode errorCode, Throwable cause) {
    super(HttpStatus.NOT_FOUND, errorCode, cause);
  }

  /**
   * Construct a new instance of the exception with the builder.
   * 
   * @param builder used to create this exception
   * @return BadRequestException
   */
  static NotFoundException newInstance(NotFoundException.Builder builder) {
    NotFoundException notFoundException = new NotFoundException(builder.errorCode,
        builder.cause);
    if (ArrayUtils.isNotEmpty(builder.getDetails())) {
      notFoundException.setDetails(builder.getDetails());
    }
    return notFoundException;
  }

  /**
   * Builder to create an NotFoundException.
   * 
   * @return UnauthorizedException.Builder
   */
  public static Builder builder() {
    return new NotFoundException.Builder();
  }

  /**
   * Builder to create an not found exception with the specific return code.
   * 
   * To use this class, use the builder to set the specific error:
   * 
   * [source,java] -- return NotFoundException .builder() .expired() .cause(new
   * NullPointerException("NPE")) .details("Could not parse currency"); --
   * 
   * @author prathapb
   *
   */
  public static class Builder
      extends WmsGlobalException.AbstractBuilder<NotFoundException, Builder> {

    /**
     * The ID(s) specified in the URL do not correspond to the values in the system.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#ENTITY_NOT_FOUND
     */
    public Builder entityNotFound() {
      return errorCode(CommonErrorCode.ENTITY_NOT_FOUND);
    }

    /**
     * Your client reached our application but we were unable to service your request due to an
     * invalid URL.
     * 
     * @return Builder to optionally set other items if required
     * @see CommonErrorCode#URI_NOT_FOUND
     */
    public Builder uriNotFound() {
      return errorCode(CommonErrorCode.URI_NOT_FOUND);
    }

    /**
     * Sets the appropriate return code when constructing the exception.
     * 
     * @return a BadRequestException with the correct error code
     */
    @Override
    public NotFoundException build() {
      return newInstance(this);
    }

  }

}
